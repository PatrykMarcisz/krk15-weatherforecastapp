package pl.academy.sda.backend.repository.open.weather.map.json;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import pl.academy.sda.backend.repository.open.weather.map.json.open.weather.api.WeatherCity;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ParseJsonTest {

    @Test
    @DisplayName("should convert from json to WeatherCity list")
    void shouldConvertFromJsonToWeatherCityList() throws IOException {
        File file = new File("src/test/resources/json/test-city-list.json");
        ObjectMapper objectMapper = new ObjectMapper();
        TypeReference<List<WeatherCity>> typeReference = new TypeReference<List<WeatherCity>>() {};
        List<WeatherCity> weatherCities = objectMapper.readValue(file, typeReference);

        assertEquals(2, weatherCities.size());

        WeatherCity firstElement = weatherCities.get(0);
        assertEquals(new Integer(707860), firstElement.getId());
        assertEquals("Hurzuf", firstElement.getName());
        assertEquals("UA", firstElement.getCountry());
        assertEquals(new Double("34.283333"), firstElement.getCoordinates().getLongitude());
        assertEquals(new Double("44.549999"), firstElement.getCoordinates().getLatitude());

        WeatherCity secondElement = weatherCities.get(1);
        assertEquals(new Integer(519188), secondElement.getId());
        assertEquals("Novinki", secondElement.getName());
        assertEquals("RU", secondElement.getCountry());
        assertEquals(new Double("37.666668"), secondElement.getCoordinates().getLongitude());
        assertEquals(new Double("55.683334"), secondElement.getCoordinates().getLatitude());
    }
}