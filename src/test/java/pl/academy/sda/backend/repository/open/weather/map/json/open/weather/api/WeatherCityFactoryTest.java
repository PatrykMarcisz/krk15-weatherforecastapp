package pl.academy.sda.backend.repository.open.weather.map.json.open.weather.api;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import pl.academy.sda.backend.repository.open.weather.map.json.open.weather.api.WeatherCityFactory;

import java.io.File;

import static org.junit.jupiter.api.Assertions.*;

class WeatherCityFactoryTest {

    @Test
    @DisplayName("file should exist")
    void fileShouldExist(){
        String pathToFile = WeatherCityFactory.PATH_TO_FILE;
        File file = new File(pathToFile);
        assertTrue(file.exists());
    }

}