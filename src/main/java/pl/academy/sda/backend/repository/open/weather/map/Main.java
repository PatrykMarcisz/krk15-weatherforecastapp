package pl.academy.sda.backend.repository.open.weather.map;

import pl.academy.sda.backend.repository.open.weather.map.dto.WeatherInfoData;
import pl.academy.sda.backend.repository.open.weather.map.json.open.weather.api.WeatherCity;
import pl.academy.sda.backend.repository.open.weather.map.service.Service;

import java.util.List;

public class Main {

    public static void main(String[] args) {
        Service service = new Service();
        List<WeatherCity> listOfCities = service.getAllCities();
        WeatherCity krakow = listOfCities.stream().filter(city -> city.getName().equals("Krakow")).findFirst().get();
        WeatherInfoData responseForCity = service.getResponseForCity(krakow);
        System.out.println(responseForCity);
   }
}
