package pl.academy.sda.backend.repository.open.weather.map.dto.open.weather.api;

import pl.academy.sda.backend.repository.open.weather.map.dto.WeatherInfoData;
import pl.academy.sda.backend.repository.open.weather.map.json.open.weather.api.OpenWeatherApiResponse;

public class OpenWeatherInfoData implements WeatherInfoData {

    OpenWeatherApiResponse response;

    public OpenWeatherInfoData(OpenWeatherApiResponse response) {
        this.response = response;
    }

    @Override
    public String getCityName(){
        return response.getName();
    }

    @Override
    public String getTemperature() {
        return String.valueOf(response.getMain().getTemp().intValue());
    }

    @Override
    public String getPerceptibleTemperature() {
        return response.getMain().getTemp().toString();
    }

    @Override
    public String getDescription() {
        return response.getWeather().get(0).getDescription();
    }

    @Override
    public String getRain() {
        return response.getRain().getVolumeForLastHour().toString();
    }

    @Override
    public String getPressure() {
        return response.getMain().getPressure().toString();
    }

    @Override
    public String getHumidity() {
        return response.getMain().getHumidity().toString();
    }

    @Override
    public String getWind() {
        return response.getWind().getSpeed().toString();
    }

    @Override
    public String getCloudiness() {
        return response.getClouds().getAll().toString();
    }

    //TODO need validate this information in someway
    @Override
    public String getSnow() {
        return "0.0";
    }

    @Override
    public String toString() {
        return "OpenWeatherInfoData{" +
                "temperature='" + getTemperature() + '\'' +
                ", perceptibleTemperature='" + getPerceptibleTemperature() + '\'' +
                ", description='" + getDescription() + '\'' +
                ", rain='" + getRain() + '\'' +
                ", pressure='" + getPressure() + '\'' +
                ", humidity='" + getHumidity() + '\'' +
                ", wind='" + getWind() + '\'' +
                ", cloudiness='" + getCloudiness() + '\'' +
                ", snow='" + getSnow() + '\'' +
                '}';
    }
}
