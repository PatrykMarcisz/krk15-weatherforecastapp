package pl.academy.sda.backend.repository.open.weather.map.repository;

import pl.academy.sda.backend.repository.open.weather.map.dto.WeatherInfoData;

public interface WeatherRepository {

    WeatherInfoData searchByCityId(Integer cityId);

}
