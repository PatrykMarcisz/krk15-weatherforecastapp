package pl.academy.sda.backend.repository.open.weather.map.json.open.weather.api;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Rain {
    @JsonProperty("1h")
    private Double volumeForLastHour;
    @JsonProperty("3h")
    private Double volumeForLastTheeHours;

    public Double getVolumeForLastHour() {
        return volumeForLastHour;
    }

    public void setVolumeForLastHour(Double volumeForLastHour) {
        this.volumeForLastHour = volumeForLastHour;
    }

    public Double getVolumeForLastTheeHours() {
        return volumeForLastTheeHours;
    }

    public void setVolumeForLastTheeHours(Double volumeForLastTheeHours) {
        this.volumeForLastTheeHours = volumeForLastTheeHours;
    }
}
