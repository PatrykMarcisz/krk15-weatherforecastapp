package pl.academy.sda.backend.repository.open.weather.map.repository.open.weather.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClients;
import pl.academy.sda.backend.repository.open.weather.map.dto.open.weather.api.OpenWeatherInfoData;
import pl.academy.sda.backend.repository.open.weather.map.dto.WeatherInfoData;
import pl.academy.sda.backend.repository.open.weather.map.json.open.weather.api.OpenWeatherApiResponse;
import pl.academy.sda.backend.repository.open.weather.map.repository.WeatherRepository;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Scanner;

public class OpenWeatherApi implements WeatherRepository {

    static final String API_BASE_URL = "https://openweathermap.org/data/2.5/weather";
    static final String API_KEY = "b6907d289e10d714a6e88b30761fae22";

    private final HttpClient httpClient;

    public OpenWeatherApi() {
        httpClient = HttpClients.createDefault();
    }

    @Override
    public WeatherInfoData searchByCityId(Integer cityId) {
        return new OpenWeatherInfoData(getResponse(cityId));
    }

    private OpenWeatherApiResponse getResponse(Integer cityId) {
        try {
            URI uri = new URIBuilder(API_BASE_URL)
                    .addParameter("id", String.valueOf(cityId))
                    .addParameter("appid", API_KEY)
                    .addParameter("units", "metric")
                    .build();
            System.out.println(uri.toString());
            HttpGet httpGet = new HttpGet(uri);
            HttpResponse response = httpClient.execute(httpGet);
            InputStream contentOfResponse = response.getEntity().getContent();
            Scanner s = new Scanner(contentOfResponse).useDelimiter("\\A");
            String jsonAsString = s.hasNext() ? s.next() : "";
            System.out.println(jsonAsString);
            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.readValue(jsonAsString, OpenWeatherApiResponse.class);
        } catch (URISyntaxException | IOException e) {
            e.printStackTrace();
        }
        throw new RuntimeException("cannot parse response");
    }
}
