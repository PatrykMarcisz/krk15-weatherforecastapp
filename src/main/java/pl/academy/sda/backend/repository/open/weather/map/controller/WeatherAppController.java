package pl.academy.sda.backend.repository.open.weather.map.controller;

import javafx.util.Pair;
import pl.academy.sda.backend.repository.open.weather.map.dto.WeatherInfoData;
import pl.academy.sda.backend.repository.open.weather.map.service.Service;

import java.util.ArrayList;
import java.util.List;

public class WeatherAppController {

    Service service = new Service();

    public List<Pair<String, String>> prepareDataForView(Integer cityId){
        WeatherInfoData responseForCity = service.getResponseForCity(cityId);
        return prepareData(responseForCity);
    }

    private List<Pair<String, String>> prepareData(WeatherInfoData responseForCity) {
        List<Pair<String,String>> data = new ArrayList<>();
        data.add(new Pair<>(PlaceholdersDefinitions.TEMPERATURE_PLACEHOLDER, responseForCity.getTemperature()));
        data.add(new Pair<>(PlaceholdersDefinitions.CITY_NAME_PLACEHOLDER, responseForCity.getCityName()));
        data.add(new Pair<>(PlaceholdersDefinitions.DESCRIPTION_PLACEHOLDER, responseForCity.getDescription()));
        data.add(new Pair<>(PlaceholdersDefinitions.RAIN_PLACEHOLDER, responseForCity.getTemperature()));
        data.add(new Pair<>(PlaceholdersDefinitions.PRESSURE_PLACEHOLDER, responseForCity.getPressure()));
        data.add(new Pair<>(PlaceholdersDefinitions.HUMIDITY_PLACEHOLDER, responseForCity.getHumidity()));
        data.add(new Pair<>(PlaceholdersDefinitions.WIND_PLACEHOLDER, responseForCity.getWind()));
        data.add(new Pair<>(PlaceholdersDefinitions.CLOUDNINESS_PLACEHOLDER, responseForCity.getCloudiness()));
        data.add(new Pair<>(PlaceholdersDefinitions.SNOW_PLACEHOLDER, responseForCity.getSnow()));
        return data;
    }

}
