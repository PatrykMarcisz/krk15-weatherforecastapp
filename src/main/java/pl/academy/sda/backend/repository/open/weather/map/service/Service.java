package pl.academy.sda.backend.repository.open.weather.map.service;

import pl.academy.sda.backend.repository.open.weather.map.dto.WeatherInfoData;
import pl.academy.sda.backend.repository.open.weather.map.json.open.weather.api.WeatherCity;
import pl.academy.sda.backend.repository.open.weather.map.json.open.weather.api.WeatherCityFactory;
import pl.academy.sda.backend.repository.open.weather.map.repository.WeatherRepository;
import pl.academy.sda.backend.repository.open.weather.map.repository.open.weather.api.OpenWeatherApi;

import java.util.List;

public class Service {

    private final WeatherRepository weatherRepository;

    public Service() {
        weatherRepository = new OpenWeatherApi();
    }

    public List<WeatherCity> getAllCities(){
        return WeatherCityFactory.getAllWeatherCities();
    }

    public WeatherInfoData getResponseForCity(WeatherCity city){
        return weatherRepository.searchByCityId(city.getId());
    }

    public WeatherInfoData getResponseForCity(Integer cityId){
        return weatherRepository.searchByCityId(cityId);

    }

}
