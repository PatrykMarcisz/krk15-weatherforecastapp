package pl.academy.sda.backend.repository.open.weather.map.dto;

public interface WeatherInfoData {

    String getCityName();

    String getTemperature();

    String getPerceptibleTemperature();

    String getDescription();

    String getRain();

    String getPressure();

    String getHumidity();

    String getWind();

    String getCloudiness();

    String getSnow();

}
