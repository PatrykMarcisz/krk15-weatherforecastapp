package pl.academy.sda.backend.repository.open.weather.map.controller;

public class PlaceholdersDefinitions {
    public static final String TEMPERATURE_PLACEHOLDER = "\\{\\{temperature\\}\\}";
    public static final String CITY_NAME_PLACEHOLDER = "\\{\\{cityName\\}\\}";
    public static final String DESCRIPTION_PLACEHOLDER = "\\{\\{description\\}\\}";
    public static final String RAIN_PLACEHOLDER = "\\{\\{rain\\}\\}";
    public static final String PRESSURE_PLACEHOLDER = "\\{\\{pressure\\}\\}";
    public static final String HUMIDITY_PLACEHOLDER = "\\{\\{humidity\\}\\}";
    public static final String WIND_PLACEHOLDER = "\\{\\{wind\\}\\}";
    public static final String CLOUDNINESS_PLACEHOLDER = "\\{\\{cloudiness\\}\\}";
    public static final String SNOW_PLACEHOLDER = "\\{\\{snow\\}\\}";



}
