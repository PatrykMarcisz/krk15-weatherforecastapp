package pl.academy.sda.frontend;

import javafx.scene.image.Image;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class WeatherAppImage extends Image {
    WeatherAppImage(String file) {
        super(inputStreamFromFile(file));
    }

    private static InputStream inputStreamFromFile(String fileName) {
        File initialFile = new File("src/main/resources/frontend/".concat(fileName));
        try {
            return new FileInputStream(initialFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}