package pl.academy.sda.frontend;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.util.Pair;
import org.apache.commons.io.FileUtils;
import pl.academy.sda.backend.repository.open.weather.map.controller.WeatherAppController;
import pl.academy.sda.backend.repository.open.weather.map.json.open.weather.api.WeatherCity;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;

public class Pogodynka extends Application {

    private static final int WIDTH = 800;
    private static final int HEIGHT = 360;

    private WeatherAppController controller = new WeatherAppController();

    private WebView browser = new WebView();
    private CityChooser comboBox = new CityChooser(this);

    @Override
    public void start(Stage primaryStage) throws Exception {

        HBox root = createRoot(primaryStage, WIDTH, HEIGHT);
        GridPane gridPane = createGridPane();
        root.getChildren().addAll(gridPane);

        Scene scene = new Scene(root);
        primaryStageSettings(primaryStage, WIDTH, HEIGHT, scene);
    }

    private HBox createRoot(Stage primaryStage, int WIDTH, int HEIGHT) {
        HBox root = new HBox();
        root.setAlignment(Pos.CENTER);
        root.setMaxWidth(WIDTH);
        root.setMaxHeight(HEIGHT);
        root.setBackground(createBackgroundFromImage(primaryStage, "world-map.jpg"));
        return root;
    }

    void reconfigureWebView(Integer cityId) {
        WebEngine webEngine = browser.getEngine();
        File file = fillFile(controller.prepareDataForView(cityId));
        try {
            webEngine.load(file.toURI().toURL().toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    private File fillFile(List<Pair<String,String>> objectsToReplace){
        File file = new File("src/main/resources/frontend/html/content.html");
        try {
            String content = FileUtils.readFileToString(new File(file.getPath()), StandardCharsets.UTF_8);
            for(Pair<String, String> pair : objectsToReplace){
                content = content.replaceAll(pair.getKey(), pair.getValue());
            }
            File fileToSave = new File("src/main/resources/frontend/html/generated/" + LocalDateTime.now().toInstant(ZoneOffset.UTC).toEpochMilli() + ".html");
            FileUtils.writeStringToFile(fileToSave, content, StandardCharsets.UTF_8);
            return fileToSave;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }

    private GridPane createGridPane() {
        ImageView weatherGirl = new ImageView(new WeatherAppImage("weather-girl-4.png"));
        GridPane gridPane = new GridPane();
        gridPane.setGridLinesVisible(false);
        gridPane.setHgap(2);
        gridPane.setVgap(2);
        rowConstraints(gridPane);
        columnConstraints(gridPane);
        gridPane.add(weatherGirl, 0, 0, 1, 2);
        gridPane.add(comboBox, 1, 0, 1, 1);
        gridPane.add(new VBox(browser), 1,1,1,1);
        gridPane.setAlignment(Pos.CENTER);
        reconfigureWebView(3094802);
        return gridPane;
    }

    private void columnConstraints(GridPane gridPane) {
        ColumnConstraints column1 = new ColumnConstraints();
        column1.setPercentWidth(30);
        ColumnConstraints column2 = new ColumnConstraints();
        column2.setPercentWidth(70);
        gridPane.getColumnConstraints().addAll(column1, column2);
    }

    private void rowConstraints(GridPane gridPane) {
        RowConstraints row1 = new RowConstraints();
        row1.setPercentHeight(45);
        RowConstraints row2 = new RowConstraints();
        row2.setPercentHeight(55);
        gridPane.getRowConstraints().addAll(row1, row2);
    }

    private void primaryStageSettings(Stage primaryStage, int WIDTH, int HEIGHT, Scene scene) {
        primaryStage.setWidth(WIDTH);
        primaryStage.setHeight(HEIGHT);
        primaryStage.setResizable(false);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private Background createBackgroundFromImage(Stage primaryStage, String fileName) {
        return new Background(new BackgroundImage(
                new WeatherAppImage(fileName),
                null,
                null,
                null,
                new BackgroundSize(
                        primaryStage.getWidth(),
                        primaryStage.getHeight(),
                        false,
                        false,
                        false,
                        false)
        ));
    }

    public static void main(String[] args) {
        launch();
    }
}

