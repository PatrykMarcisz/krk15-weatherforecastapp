package pl.academy.sda.frontend;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.ComboBox;
import pl.academy.sda.backend.repository.open.weather.map.json.open.weather.api.WeatherCity;
import pl.academy.sda.backend.repository.open.weather.map.json.open.weather.api.WeatherCityFactory;

import java.util.List;

public class CityChooser extends ComboBox<WeatherCity> {

    public CityChooser(Pogodynka application) {
        List<WeatherCity> allWeatherCities = WeatherCityFactory.getAllWeatherCities();
        this.setPromptText("Wybierz miasto... ");
        this.getItems().addAll(allWeatherCities);
        this.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> application.reconfigureWebView(newValue.getId()));
    }
}
